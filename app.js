var express =  require('express')
var app = express()

app.use(express.static(__dirname + '/build'))
app.use(express.Router())

app.listen(8000, function() {
  console.log('Listening on 8000')
})
