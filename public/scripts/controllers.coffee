exports.home = ($scope, $http, $timeout) ->  

  weatherTypes = ['sunny.png', 'partlyCloudy.png', 'partlyCloudy.png', 'cloudy.png', 'cloudy.png', 'cloudy.png', 'partlyCloudy.png',
    'partlyCloudy.png', 'snow.png', 'heavyRain.png', 'heavyRain.png', 'heavyRain.png', 'lightRain.png', 'storm.png',
    'storm.png', 'storm.png', 'no-pic', 'snow.png', 'snow.png', 'snow.png', 'snow.png', 'snow.png', 'snow.png','snow.png' ]

  
  $scope.date = {}
  refreshDate = ->
    $scope.date = new Date()
    $timeout(refreshDate, 5000)
  refreshDate()

  $scope.weather = {}
  refreshWeather = ->
    $http(method: 'GET', url: 'http://54.88.45.37:3000/api/weather/dc')
      .success (data, status) ->
        $scope.weather.temp = data.feelslike_f
        $scope.weather.readable = data.weather
      .error (data, status) ->
    $scope.weather.hourly = {}
    $http(method: 'GET', url: 'http://54.88.45.37:3000/api/weather/hourly/dc')
      .success (data, status) ->
        console.log data
        $scope.weather.hourly.first = data[0]
        $scope.weather.hourly.first.FCTTIME.hour = $scope.weather.hourly.first.FCTTIME.civil.split(':')[0]
        $scope.weather.hourly.first.icon = weatherTypes[data[0].fctcode]

        $scope.weather.hourly.second = data[1]
        $scope.weather.hourly.second.FCTTIME.hour = $scope.weather.hourly.second.FCTTIME.civil.split(':')[0]
        $scope.weather.hourly.second.icon = weatherTypes[data[1].fctcode]

        $scope.weather.hourly.third = data[2]
        $scope.weather.hourly.third.FCTTIME.hour = $scope.weather.hourly.third.FCTTIME.civil.split(':')[0]
        $scope.weather.hourly.third.icon = weatherTypes[data[2].fctcode]

        $scope.weather.hourly.fourth = data[3]
        $scope.weather.hourly.fourth.FCTTIME.hour = $scope.weather.hourly.fourth.FCTTIME.civil.split(':')[0]
        $scope.weather.hourly.fourth.icon = weatherTypes[data[3].fctcode]


    $timeout(refreshWeather, 300000)
  refreshWeather()

  $scope.bikes = {}
  refreshBikes = ->
    $http(method: 'GET', url: 'http://54.88.45.37:3000/api/capitalbikeshare')
      .success (data, status) ->
        $scope.bikes = data
      .error (data, status) ->
    $timeout(refreshBikes, 10000)
  refreshBikes()


  $scope.metro = {}
  refreshAll = ->
    $http(method: 'GET', url: "http://54.88.45.37:3000/api/metro/prediction" )
      .success (data, status) ->
        for a in data
          $scope.metro[a.LocationCode] = []
          unless a.Min is 'BRD' or a.Min is 'ARR' then a.Min += ' min'
        for a in data 
          unless a.DestinationCode is null
            holder = 
              originName: a.LocationName
              destinationName: a.DestinationName
              destinationLetter: a.DestinationCode[0]
              min: a.Min
              line: a.Line
            $scope.metro[a.LocationCode].push(holder)
      .error (data, status) ->
    $scope.map = 
      center:
        latitude: 38.9054230
        longitude: -77.0420310
      zoom: 15
      showTraffic: true

    $timeout(refreshAll, 10000)
  refreshAll()

  

  











