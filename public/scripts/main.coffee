'use strict'

require('angular/angular')
require('angular-route/angular-route')
require('angular-resource/angular-resource')
require('angular-animate/angular-animate')

controllers = require('./controllers')

app = angular.module 'app', ['ngRoute', 'google-maps', 'fx.animations', 'ngAnimate']

app.config ($routeProvider, $locationProvider, $httpProvider) ->
  
  # Set up pretty urls
  $locationProvider.html5Mode(true)

  # Routes
  $routeProvider
    .when '/',
      templateUrl: 'refactor.html',
      controller: controllers.home
    .otherwise redirectTo: '/'

result = []
app.filter 'farragutWestB', () ->
  return (x)  ->
    result = []
    angular.forEach x, (item) ->
      if item.destinationLetter is 'G'
        result.push item
      if item.destinationLetter is 'D'
        result.push item
    return result

app.filter 'farragutWestA', () ->
  return (x)  ->
    result = []
    angular.forEach x, (item) ->
      if item.destinationLetter is 'J'
        result.push item
      if item.destinationLetter is 'K'
        result.push item    
    return result





